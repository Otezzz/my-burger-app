import React from 'react';
import Axl from '../../hoc/Axl';
import classes from './Layout.module.css';

const layout = ( props ) => (
  <Axl>
    <div>Toolbar, Sidebar, Backdrop</div>
    <main className={classes.Content}>
      {props.children}
    </main>
  </Axl>
);

export default layout;