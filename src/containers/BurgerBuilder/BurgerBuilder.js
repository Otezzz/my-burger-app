import React, { Component } from 'react';
import Axl from '../../hoc/Axl';

class BurgerBuilder extends Component {
  render () {
    return (
      <Axl>
        <div>Build Control</div>
        <div>Burger</div>
      </Axl>
    );
  }
}

export default BurgerBuilder;